\documentclass{beamer}

\usetheme[grey]{Collabora}
\usefonttheme{Collabora}

\usepackage{color}
\usepackage{graphicx}
\usepackage{ulem}
\usepackage{caption}

\definecolor{blcode}{rgb}{0.5,0.5,0.5}
\setbeamercolor{code}{fg=blcode}
\newcommand{\code}[1]{{\usebeamercolor[fg]{code} \path{#1}}}
\newcommand{\source}[1]{\caption*{Source: {#1}} }

\title{Open Source Graphics 101: Getting Started}
\author{Boris Brezillon}
\date{October 29, 2019}

%\usepackage{pgfpages}
%\setbeameroption{show notes on second screen}

\begin{document}

\maketitle

\section{Disclaimer}

\begin{frame}
\frametitle{Disclaimer}
\begin{itemize}
\item I am not an experienced Graphics developer
 \begin{itemize}
 \item Take my words with a grain of salt
 \item Please correct me if you think I'm wrong
 \end{itemize}
\item This presentation is about
 \begin{itemize}
 \item Explaining what GPUs are and how they work
 \item Giving the big picture of the Linux Open Source Graphics stack
 \item Providing some hints about Mesa and the DRM subsystem so you can dig further if interested
 \end{itemize}
\item This presentation is \textbf{not} about
 \begin{itemize}
 \item Teaching you how to develop a GPU driver
 \item Teaching you how to use Graphics APIs (OpenGL/Vulkan/D3D)
 \end{itemize}
\end{itemize}
\end{frame}

\section{The Graphics Pipeline}

\begin{frame}
  \frametitle{The Graphics Pipeline}
  \begin{center}
  \includegraphics[height=0.6\textheight]{gfx-pipeline.pdf}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{The Graphics Pipeline}
  \begin{center}
  \includegraphics[width=\textwidth]{gfx-pipeline2.pdf}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{The Geometry Stage}
  \begin{center}
  \includegraphics[height=0.7\textheight]{geometry-stage.pdf}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{The Rasterizer Stage}
  \begin{center}
  \includegraphics[height=0.7\textheight]{rasterizer-stage.pdf}
  \end{center}
\end{frame}

\section{GPUs Internals}

\begin{frame}
  \frametitle{GPU Internals}
  \begin{center}
  \includegraphics[height=0.6\textheight]{gpu-internals.pdf}
  \end{center}
\end{frame}

\begin{frame}
\frametitle{Let's go massively parallel!}
Why?
\begin{itemize}
 \item Vertices, normals, fragments can be processed independently
 \item We have a lot of them (complex scene, complex models, high resolution)
 \item We want real-time rendering
\end{itemize}
How?
\begin{itemize}
 \item SIMD (Single Instruction Multiple Data)
 \item Add cores and reduce their complexity
  \begin{itemize}
  \item Shared dedicated units for complex/specialized tasks
  \item No fancy CPU stuff like out-of-order control logic, smart pre-fetcher, branch predictors, ...
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Parallelization, how hard can it be?}
SIMD + lot of cores: we're done, right?
  \begin{figure}
  \begin{center}
  \includegraphics[height=0.5\textheight]{bpcy0zq8zfa01.jpg}
  \source{http://devhumor.com/media/multithreaded-programming}
  \end{center}
  \end{figure}
\end{frame}
\begin{frame}

\frametitle{Parallelization, how hard can it be?}
\begin{itemize}
 \item Stalls caused by memory access
 \begin{itemize}
  \item Add caches
  \item Multi-threading
 \end{itemize}
 \item SIMD: try to get all ALUs busy
 \begin{itemize}
  \item Avoid conditional branches
  \item Try to pack similar operation together
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Suboptimal ALU utilization: Memory accesses}
  \begin{center}
  \includegraphics[height=0.5\textheight]{memory-stalls.pdf}
  \end{center}
\end{frame}

\begin{frame}
\frametitle{How to avoid load/store stalls? Multi-threading}
  \begin{center}
  \includegraphics[height=0.6\textheight]{threads.pdf}
  \end{center}
\end{frame}

\begin{frame}
\frametitle{SIMD + conditional branches}
  \begin{center}
  \includegraphics[height=0.45\textheight]{simd.pdf}
  \end{center}
\end{frame}

\section{Interacting with your GPU}

\begin{frame}
\frametitle{CPU: Hey GPU, listen/talk to me please!}
\begin{itemize}
 \item The CPU is in charge of all apps running on a machine, including graphics apps
 \item The CPU needs a way to send requests to/get results from the GPU
 \item Huge amount of data needs to be exchanged (vertices, framebuffers, textures, ...)
 \item How?
 \begin{itemize}
  \item Put everything in memory
  \item Set of operations to execute is also stored in memory (frequently called command stream)
  \item Once everything is in memory, ask the GPU to execute what we prepared
  \item Let the GPU inform us when it's done
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{GPU Command Stream}
\begin{center}
 \includegraphics[width=\textwidth]{cmdstream.pdf}
\end{center}
\end{frame}

\section{The Linux Graphics Stack}

\begin{frame}
\frametitle{The Big Picture}
\begin{center}
 \includegraphics[height=0.7\textheight]{linux-gfx-stack.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{The Graphics API: What are they?}
\begin{itemize}
 \item That's what applications interact with to render things
 \item Those APIs are here to abstract the GPU pipeline configuration/manipulation
 \item You might have the choice
 \begin{itemize}
  \item OpenGL/OpenGLES: Well established, well supported, and widely used
  \item Vulkan: Modern API, this is the future, but not everyone uses/supports it yet
  \item Direct3D: Windows Graphics API (version 12 of the API resembles the Vulkan API)
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Graphics API: Shaders}
\begin{itemize}
\item Part of the pipeline is programmable: the shaders
 \begin{itemize}
 \item Separate Programming Language: GLSL or HLSL
 \item Programs are passed as part of the pipeline configuration...
 \item ... and compiled by drivers to generate hardware-specific bytecode
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Graphics API: OpenGL(ES) vs Vulkan}
\begin{itemize}
\item Two philosophies:
 \begin{itemize}
 \item OpenGL tries to hide as much as possible the GPU internals, thus making users life easier
 \item Vulkan provides fine grained control, letting users decide what is best for them
 \item Vulkan provides a way to record operations and replay them
 \item More work for the developer, less work for the CPU
 \end{itemize}
\item As a side effect, Vulkan applications are more verbose, but
 \begin{itemize}
 \item Vulkan verbosity can be leveraged by higher-level APIs
 \item Driver are simpler and more performant
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Graphics API: OpenGL(ES) vs Vulkan}
\begin{itemize}
\item OpenGL(ES)
 \begin{itemize}
 \item Big State Machine
  \begin{enumerate}
  \item Prepare data to be passed to the GPU (vertices, texture, shaders, ...)
  \item Record the state to be applied to each element of the pipeline
  \item Once everything is ready, trigger the execution
  \item Go back to 1, and do it again, and again, and again ...
  \end{enumerate}
 \item The amount of things to setup to render a complex scene can be quite important
 \end{itemize}
\item Vulkan
 \begin{itemize}
 \item The State Machine is gone
 \item Ease multi-threading support
 \item Much more efficient
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Kernel/Userspace Driver Separation}
\begin{itemize}
\item GPUs are complex beasts, incidentally, drivers are complex too
 \begin{itemize}
 \item We don't want to put all the complexity kernel side
 \item Not all code needs to run in a privileged context
 \item Debugging in userspace is much easier
 \item \sout{Licensing issues (for closed source drivers)}
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Kernel Drivers}
\begin{itemize}
\item Kernel drivers deal with
 \begin{itemize}
 \item Memory
 \item Command Stream submission/scheduling
 \item Interrupts and signaling
 \end{itemize}
\item Only drivers with open-source userspace drivers live in Linus' tree: \code{drivers/gpu/drm/}
\item Those interfacing with closed-source userspace drivers are out-of-tree
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Kernel Drivers: Memory Management}
\begin{itemize}
\item Two Frameworks
 \begin{itemize}
 \item \code{GEM}: Graphics Execution Manager
 \item \code{TTM}: Translation Table Manager
 \end{itemize}
\item GPU drivers using \code{GEM}
 \begin{itemize}
 \item Should provide an \code{ioctl()} to allocate Buffer Objects (\code{BO}s)
 \item Releasing \code{BO}s is done through a generic \code{ioctl()}
 \item Might provide a way to do cache maintenance operations on a \code{BO}
 \item Should guarantee that \code{BO}s referenced by a submitted Command Stream are
       properly mapped GPU-side
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Kernel Drivers: Scheduling}
\begin{itemize}
\item Submission != Immediate execution
 \begin{itemize}
 \item Several processes might be using the GPU in parallel
 \item The GPU might already be executing other \code{cmdstream}s when the request comes in
 \end{itemize}
\item Submission == Queue the \code{cmdstream}
\item Each driver has its own \code{ioctl()} for that
\item Userspace driver knows inter-cmdstream dependencies
\item Scheduler needs to know about those constraints too
\item \code{DRM} provides a generic scheduling framework: \code{drm_sched}
\item Should be used by all drivers to implement their scheduling logic
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Userspace/Kernel Driver Synchronization}
\begin{itemize}
\item Userspace driver needs to know when the GPU is done executing a \code{cmdstream}
\item Hardware reports that through an \code{interrupt}
\item Information has to be propagated to userspace
\item Here come \code{fence}s: objects allowing one to wait on job completion
\item Exposed as \code{syncobj}s objects to userspace
\item \code{fence}s can also be placed on \code{BO}s
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Userspace Driver: Roles}
\begin{itemize}
\item Probably one of the most complex element of the chain
\item Userspace driver is in charge of
 \begin{itemize}
 \item Exposing one or several Graphics API
  \begin{itemize}
  \item Maintaining the API specific state machine (if any)
  \item Managing off-screen rendering contexts (if any)
  \item Compiling shaders into hardware specific bytecode
  \item Creating, populating and submitting command streams
  \end{itemize}
 \item Interacting with the Windowing System
  \begin{itemize}
  \item Managing on-screen rendering contexts
  \item Binding/unbinding render buffers
  \item Synchronizing on render operations
  \end{itemize}
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Mesa: Open Source Userspace Drivers}
\begin{itemize}
\item Mesa provides a frontend for GL APIs (libGL(ES))
 \begin{itemize}
 \item GL calls are dispatched to the appropriate driver
 \item Drivers are shared libs loaded on demand
 \end{itemize}
\item D3D9 support is provided through a mesa D3D driver
 \begin{itemize}
 \item Based on the Gallium abstraction
 \item D3D calls get redirected to a Gallium driver
 \end{itemize}
\item Vulkan has its own Open Source loader
 \begin{itemize}
 \item Mesa just provides Vulkan drivers
 \item No Gallium-like abstraction for Vulkan drivers
 \item Code sharing through libs
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Mesa State Tracking: Pre-Gallium}
\begin{center}
 \includegraphics[height=0.7\textheight]{linux-gfx-stack-dri.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Mesa State Tracking: Gallium}
\begin{center}
 \includegraphics[height=0.7\textheight]{linux-gfx-stack-gallium.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Mesa State Tracking: Vulkan}
\begin{center}
 \includegraphics[height=0.7\textheight]{linux-gfx-stack-vk.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Mesa: Shader Compilation}
\begin{itemize}
\item Compilation is a crucial aspect
\item GPU efficiency is heavily dependent on compiler optimizations
\item Compilation usually follows the following steps
 \begin{itemize}
 \item Shader Programming Language -> Generic Intermediate Representation (IR)
 \item Optimization in the generic IR space
 \item Generic IR -> GPU specific IR
 \item Optimization in the GPU specific IR space
 \item Byte code generation
 \end{itemize}
\item Note that you can have several layers of generic IR
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Mesa: Shader Compilation}
\begin{center}
 \includegraphics[height=0.6\textheight]{shader-compilation.pdf}
\end{center}
\end{frame}

\section{Conclusion}

\begin{frame}
\frametitle{Nice overview, but what's next?}
\begin{itemize}
\item The GPU topic is quite vast
\item Start small
 \begin{itemize}
 \item Choose a driver
 \item Find a feature that's missing or buggy
 \item Stick to it until you get it working
 \end{itemize}
\item Getting a grasp on GPU concept/implementation takes time
\item Don't give up
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Useful readings}
\begin{itemize}
\item Understanding how GPUs work is fundamental:
 \begin{itemize}
 \item \url{https://fgiesen.wordpress.com/2011/07/09/a-trip-through-the-graphics-pipeline-2011-index/}
 \item \url{https://www.cs.cmu.edu/afs/cs/academic/class/15462-f11/www/lec_slides/lec19.pdf}
 \item Search "how GPUs work" on Google  ;-)
 \end{itemize}
\item Mesa source tree is sometimes hard to follow, refer to the doc:
 \url{https://mesa-docs.readthedocs.io/en/latest/sourcetree.html}
\item And the DRM kernel doc can be useful too:
 \url{https://01.org/linuxgraphics/gfx-docs/drm/gpu/index.html}
\end{itemize}
\end{frame}

\thanks{Q \& A \\(Psst, We're hiring!)}

\end{document}

