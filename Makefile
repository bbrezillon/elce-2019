SOURCES = $(wildcard *.tex)
DIAGRAMS = gfx-pipeline.pdf \
	   gfx-pipeline.svg \
       gfx-pipeline2.pdf \
       gfx-pipeline2.svg \
       geometry-stage.pdf \
       geometry-stage.svg \
       rasterizer-stage.pdf \
       rasterizer-stage.svg \
       gpu-internals.pdf \
       gpu-internals.svg \
       cmdstream.pdf \
       cmdstream.svg \
       linux-gfx-stack.pdf \
       linux-gfx-stack.svg \
       linux-gfx-stack-gallium.pdf \
       linux-gfx-stack-gallium.svg \
       linux-gfx-stack-dri.pdf \
       linux-gfx-stack-dri.svg \
       linux-gfx-stack-vk.pdf \
       linux-gfx-stack-vk.svg \
       shader-compilation.pdf \
       shader-compilation.svg \
       simd.pdf \
       simd.svg \
       memory-stalls.pdf \
       memory-stalls.svg \
       threads.pdf \
       threads.svg

PDFS = $(SOURCES:.tex=.pdf)

all: $(DIAGRAMS) $(PDFS)

%.pdf: %.tex
	xelatex $<
	xelatex $<
	xelatex $<

%.pdf: %.eps
	epstopdf --outfile=$@ $^

%.eps: %.dia
	dia -e $@ -t eps $^

%.svg: %.dia
	dia -e $@ -t svg $^

clean:
	rm -f *.aux *.log *.nav *.out *.snm *.toc

distclean: clean
	rm -f $(PDFS)

present: $(firstword $(PDFS))
	pdf-presenter-console -g $< $(shell grep -q '^.setbeameroption.show notes' $(<:.pdf=.tex) && echo --notes=right)

.PHONY: all clean
